#pragma checksum "g:\STC\BankManagement\Pages\BankLog.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d957e4effb2da0bc75d3237a05ddafaeba18f260"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(BankManagement.Pages.Pages_BankLog), @"mvc.1.0.razor-page", @"/Pages/BankLog.cshtml")]
namespace BankManagement.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "g:\STC\BankManagement\Pages\_ViewImports.cshtml"
using BankManagement;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "g:\STC\BankManagement\Pages\BankLog.cshtml"
using Database;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d957e4effb2da0bc75d3237a05ddafaeba18f260", @"/Pages/BankLog.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"52f3a22d538b5b646fc89b0887ae976decc934f6", @"/Pages/_ViewImports.cshtml")]
    public class Pages_BankLog : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "g:\STC\BankManagement\Pages\BankLog.cshtml"
  
    ViewData["Title"] = "Bank log page";
    ViewData["User"] = User.Identity.Name;

    string printToPage() {
        if(User.Identity.IsAuthenticated){
            return "Please, log out!";
        }

        if(User.Identity.Name != null && UserDatabase.usernames.ContainsKey(User.Identity.Name)) {
            return "Please, log out!";
        }

        string full = "";
        foreach(int acc in DatabaseConnection.getAccounts()) {
            Stack<DbLog> log = DatabaseConnection.getTransferLog(acc);
            string s = "<h2 class='display-4'>User Account: " + acc + "</h2><ul class='list-group'>"; 
            foreach(DbLog l in log) {
                s += "<li class='list-group-item'>Action: <strong>" + l.info + "</strong> | Change in money: <a class=" 
                + (l.money >= 0 ? "'text-success'" : "'text-danger'") + ">" + (l.money >= 0 ? ("+" + l.money) : l.money + "") 
                + "</a> | Time and Date of action: " + l.time.ToString() + "</li>";
            }
            s += "</ul></br>";
            full += s;
        }
        return full;
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"text-center\">\r\n    <h1 class=\"display-3\">Your bank Log</h1>\r\n    ");
#nullable restore
#line 35 "g:\STC\BankManagement\Pages\BankLog.cshtml"
Write(Html.Raw(printToPage()));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<BankLogModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<BankLogModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<BankLogModel>)PageContext?.ViewData;
        public BankLogModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
