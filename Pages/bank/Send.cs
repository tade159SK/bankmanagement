﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Database;
using System;

namespace BankManagement
{
    public class SendModel : PageModel
    {
        private readonly ILogger<SendModel> _logger;

        public SendModel(ILogger<SendModel> logger)
        {
            _logger = logger;
        }

        [BindProperty]
        public string Money { get; set; }

        [BindProperty]
        public string AccountId { get; set; }

        public IActionResult OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page(); 
            }

            double amount = 0;
            int accId = 0;

            if(!double.TryParse(Money, out amount)){
                ModelState.AddModelError("", "Please, use valid number for money! Decimal numbers use with comma! Example: 0,1");
                return Page(); 
            }

            if(!int.TryParse(AccountId, out accId)){
                ModelState.AddModelError("", "Please, use valid account number!");
                return Page(); 
            }

            if(!DatabaseConnection.isAccountValid(accId)) {
                ModelState.AddModelError("", "An account you are trying to send is not valid!");
                return Page(); 
            }

            amount = Math.Round(amount, 2);

            if(amount == 0.00) {
                ModelState.AddModelError("", "Please, use valid number for money!");
                return Page(); 
            }

            Account account = UserDatabase.usernames[User.Identity.Name];

            if(accId == account.accountId) {
                ModelState.AddModelError("", "You can't send money to yourself!");
                return Page(); 
            }

            double currentMoney = DatabaseConnection.getAmount(account.accountId);

            if(currentMoney < (amount + account.getSendFee())) {
                ModelState.AddModelError("", "Not enough money in account!");
                return Page(); 
            }

            ModelState.AddModelError("", "Money were sent! Sent money: " + amount + "$ Fee: " + account.getSendFee() + "$");
            DatabaseConnection.transferMoney(account.accountId, "Send to account " + accId, -amount);
            DatabaseConnection.transferMoney(accId, "From account " + account.accountId, amount);
            DatabaseConnection.transferMoney(account.accountId, "Send Fee", -account.getSendFee());
            return Page();
        }
    }
}
