﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Database;
using System;

namespace BankManagement
{
    public class WithdrawModel : PageModel
    {
        private readonly ILogger<WithdrawModel> _logger;

        public WithdrawModel(ILogger<WithdrawModel> logger)
        {
            _logger = logger;
        }

        [BindProperty]
        public string Money { get; set; }

        public IActionResult OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page(); 
            }

            double amount = 0;

            if(!double.TryParse(Money, out amount)){
                ModelState.AddModelError("", "Please, use valid number! Decimal numbers use with comma! Example: 0,1");
                return Page(); 
            }

            amount = Math.Round(amount, 2);

            if(amount == 0.00) {
                ModelState.AddModelError("", "Please, use valid number!");
                return Page(); 
            }

            Account account = UserDatabase.usernames[User.Identity.Name];
            double currentMoney = DatabaseConnection.getAmount(account.accountId);

            if(currentMoney < (amount + account.getWithdrawFee())) {
                ModelState.AddModelError("", "Not enough money in account!");
                return Page(); 
            }

            ModelState.AddModelError("", "Money were withdrawn! Withdrawn money: " + amount + "$ Fee: " + account.getWithdrawFee() + "$");
            DatabaseConnection.transferMoney(account.accountId, "Withdraw", -amount);
            DatabaseConnection.transferMoney(account.accountId, "Withdraw Fee", -account.getWithdrawFee() );
            return Page();
        }
    }
}
