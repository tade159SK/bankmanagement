﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Database;
using System;

namespace BankManagement
{
    public class DepositModel : PageModel
    {
        private readonly ILogger<DepositModel> _logger;

        public DepositModel(ILogger<DepositModel> logger)
        {
            _logger = logger;
        }

        [BindProperty]
        public string Money { get; set; }

        public IActionResult OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page(); 
            }

            double amount = 0;

            if(!double.TryParse(Money, out amount)){
                ModelState.AddModelError("", "Please, use valid number! Decimal numbers use with comma! Example: 0,1");
                return Page(); 
            }

            amount = Math.Round(amount, 2);

            if(amount == 0.00) {
                ModelState.AddModelError("", "Please, use valid number!");
                return Page(); 
            }

            Account account = UserDatabase.usernames[User.Identity.Name];
            double currentMoney = DatabaseConnection.getAmount(account.accountId);

            ModelState.AddModelError("", "Money were disposed! Disposed money: " + amount + "$ Fee: " + account.getDepositFee() + "$");
            DatabaseConnection.transferMoney(account.accountId, "Deposit", amount);
            DatabaseConnection.transferMoney(account.accountId, "Deposit Fee", -account.getDepositFee() );
            return Page();
        }
    }
}
