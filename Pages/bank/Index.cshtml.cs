using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Models;

namespace BankManagement.Pages.bank
{
    public class BankIndexModel : PageModel
    {
        private readonly ILogger<BankIndexModel> _logger;

        public BankIndexModel(ILogger<BankIndexModel> logger)
        {
            _logger = logger;
        }
    }
}