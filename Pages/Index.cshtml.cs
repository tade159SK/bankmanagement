﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Models;
using Database;
using System;
using Database.Accounts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace BankManagement
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        [BindProperty]
        public Customer customer { get; set; }

        public IActionResult OnGet() {
            if (User.Identity.IsAuthenticated) {
                return Redirect("./bank/Index");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (User.Identity.IsAuthenticated) {
                return Redirect("./bank/Index");
            }

            if (!ModelState.IsValid)
            {
                return Page(); 
            }

            int Id = Database.DatabaseConnection.getUserId(customer.Name, customer.Password);
            if (Id <= 0)
            {
                ModelState.AddModelError("", "Username or password is invalid");
                return Page();
            }
            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, customer.Name));
            identity.AddClaim(new Claim(ClaimTypes.Name, customer.Name));
            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties 
                { IsPersistent = false });

            string aT = Database.DatabaseConnection.getAccountType(Id);
            eAccountType accountType = eAccountType.Normal.ToString().Equals(aT) ? eAccountType.Normal : eAccountType.Student;

            Account account = null;
            if (accountType == eAccountType.Normal)
                account = new NormalAccount(customer.Name, Id, accountType);
            else 
                account = new StudentAccount(customer.Name, Id, accountType);

            if(UserDatabase.usernames.ContainsKey(customer.Name))
                UserDatabase.usernames.Remove(customer.Name);

            UserDatabase.usernames.Add(account.username, account);
            return Redirect("./bank/Index");
        }
    }
}
