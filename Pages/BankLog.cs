using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Models;

namespace BankManagement.Pages
{
    public class BankLogModel : PageModel
    {
        private readonly ILogger<BankLogModel> _logger;

        public BankLogModel(ILogger<BankLogModel> logger)
        {
            _logger = logger;
        }
    }
}