﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication;
using Database;

namespace BankManagement
{
    public class LogoutModel : PageModel
    {
        private readonly ILogger<LogoutModel> _logger;

        public LogoutModel(ILogger<LogoutModel> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            UserDatabase.usernames.Remove(User.Identity.Name);
            await HttpContext.SignOutAsync();
            return RedirectToPage("Index");
        }

        public async Task<IActionResult> OnGetAsync()
        {
            UserDatabase.usernames.Remove(User.Identity.Name);
            await HttpContext.SignOutAsync();
            return RedirectToPage("Index");
        }
    }
}
