namespace Database.Accounts {

    public class NormalAccount : Account {

        public NormalAccount(string username, int accountId, eAccountType accountType) : base(username, accountId, accountType) { }

        public override double getDepositFee() {
            return 0.2;
        }

        public override double getWithdrawFee() {
            return 0.1;
        }

        public override double getSendFee() {
            return 0.05;
        }
    }
}