namespace Database.Accounts {

    public class StudentAccount : Account {

        public StudentAccount(string username, int accountId, eAccountType accountType) : base(username, accountId, accountType) { }

        public override double getDepositFee() {
            return 0.0;
        }

        public override double getWithdrawFee() {
            return 0.0;
        }

        public override double getSendFee() {
            return 0.0;
        }
    }
}