namespace Database {

    public abstract class Account {

        public string username {get; }
        public int accountId {get; }
        public eAccountType accountType {get; }

        public Account(string username, int accountId, eAccountType accountType) {
            this.username = username;
            this.accountId = accountId;
            this.accountType = accountType;
        }

        public abstract double getWithdrawFee();
        public abstract double getDepositFee();
        public abstract double getSendFee();
    }
}