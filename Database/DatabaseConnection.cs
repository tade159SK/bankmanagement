using System.Data;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using MySql.Data.Types;

namespace Database {

    public class DatabaseConnection {

        public static String CONNECTION_STRING = "Server=sql7.freesqldatabase.com;Database=sql7309808;Uid=sql7309808;Pwd=cZ888NUdng";

        public static int getUserId(string username, string password) {
            using(MySqlConnection connection = new MySqlConnection(CONNECTION_STRING)) {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT id from accounts WHERE username = @username AND password = PASSWORD(@password)", connection);
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@password", password);
                MySqlDataReader reader = cmd.ExecuteReader();
                if(reader.Read())
                    return reader.GetInt32(0);
                return 0;
            }
        }

        public static string getAccountType(int accountId) {
            using(MySqlConnection connection = new MySqlConnection(CONNECTION_STRING)) {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT accountType from accounts WHERE id = @ID", connection);
                cmd.Parameters.AddWithValue("@ID", accountId);
                MySqlDataReader reader = cmd.ExecuteReader();
                if(reader.Read())
                    return reader.GetString(0);
                return "Normal";
            }
        }

        public static bool isAccountValid(int accountId) {
            using(MySqlConnection connection = new MySqlConnection(CONNECTION_STRING)) {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT id from accounts WHERE id = @ID", connection);
                cmd.Parameters.AddWithValue("@ID", accountId);
                MySqlDataReader reader = cmd.ExecuteReader();
                return reader.Read();
            }
        }

        public static List<int> getAccounts() {
            using(MySqlConnection connection = new MySqlConnection(CONNECTION_STRING)) {
                List<int> accounts = new List<int>();
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT id from accounts", connection);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read()) {
                    accounts.Add(reader.GetInt32(0));
                }
                return accounts;
            }
        }

        public static double getAmount(int accountId) {
            using(MySqlConnection connection = new MySqlConnection(CONNECTION_STRING)) {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT SUM(amount) FROM `transfer_log` WHERE `accountId`= @ID", connection);
                cmd.Parameters.AddWithValue("@ID", accountId);
                MySqlDataReader reader = cmd.ExecuteReader();
                if(reader.Read()){
                    MySqlDecimal dec = reader.GetMySqlDecimal(0);
                    if(dec.IsNull) {
                        return 0.0;
                    }
                    return dec.ToDouble();
                }
                return 0.0;
            }
        }

        public static Stack<DbLog> getTransferLog(int accountId) {
            using(MySqlConnection connection = new MySqlConnection(CONNECTION_STRING)) {
                connection.Open();
                Stack<DbLog> log = new Stack<DbLog>();
                MySqlCommand cmd = new MySqlCommand("SELECT info, amount, timestamp FROM `transfer_log` WHERE `accountId`= @ID", connection);
                cmd.Parameters.AddWithValue("@ID", accountId);
                MySqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read()){
                    log.Push(new DbLog(accountId, reader.GetString(0), reader.GetMySqlDecimal(1).ToDouble(), reader.GetMySqlDateTime(2).GetDateTime()));
                }
                return log;
            }
        }

        public static void transferMoney(int accountId, string info, double amount) {
            using(MySqlConnection connection = new MySqlConnection(CONNECTION_STRING)) {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO `transfer_log` (`accountId`, `info`, `amount`) VALUES (@ID, @info, @amount)", connection);
                cmd.Parameters.AddWithValue("@ID", accountId);
                cmd.Parameters.AddWithValue("@info", info);
                cmd.Parameters.AddWithValue("@amount", amount);
                cmd.ExecuteNonQuery();
            }
        }
    }
}