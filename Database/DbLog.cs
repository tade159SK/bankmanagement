using System;

namespace Database {

    public class DbLog {

        public int accountId {get; set;}
        public string info {get; set;}
        public double money {get; set;}
        public DateTime time {get; set;}

        public DbLog(int accountId, string info, double money, DateTime time) {
            this.accountId = accountId;
            this.info = info;
            this.money = money;
            this.time = time;
        }
    }
}